# Installation

To install the requirements, simply

```
virtualenv -p python3 venv
source venv/bin/activate
python -m pip install -r requirements.txt
```

And then you are ready to go

# Running experiments

To run an experiment :

	python train.py --model FancyCNN --data_augment --normalize

To run experiments with NeptuneAI :

	export NEPTUNE_TOKEN=YOUR_NEPTUNE_TOKEN
	export NEPTUNE_PROJECT=YOUR_NEPTUNE_PROJECT
	python train.py --model FancyCNN --data_augment --normalize

You can view some runs on the [public neptune.ai
dashboard](https://app.neptune.ai/jeremyfix/CamexIA-FashionMNIST/experiments?split=tbl&dash=charts&viewId=97c07a2b-0691-46f1-bc2b-70be9f32fb1d)

# Running the experiments on colab

If you want, you can take the notebook of this repository and upload it on google colab to run an experiment. The notebook contain all the required code to install the necessary dependencies. 

Warning: on colab, there are already preinstalled package that may conflict with our requirements and, for now, it is not completely clear how to setup a virtualenv within a jupyter notebook.

# Running the experiments in a local jupyter notebook with a virtual environment

If you want to run locally the experiments, in a virtual environment as defined above, you must add the virtual environment to the jupyter notebook.

To do so, we first need to install an additional package :

```
source venv/bin/activate
python -m pip install ipykernel
```

We also install locally jupyter notebook

```
python -m pip install notebook
```

We can then register the virtualenv into jupyter :

```
python -m ipykernel install --user --name Camexia --display-name "Python (TP Camexia)"
```

Running jupyter, you should see "Python (TP Camexia)" in the list of "Kernel/Change Kernel".

```
jupyter notebook
```
