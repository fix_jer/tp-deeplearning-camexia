

import random
from tqdm import trange
import time
l = range(500)

class ProgressBar:

    def __init__(self, tot):
        self.tot = tot
        self.enter()

    def enter(self):
        self.myrange=trange(self.tot)
        self.iter = iter(self.myrange)

    def __call__(self, msg):
        if self.iter is None:
            self.enter()
        j = next(self.iter)
        if j == self.tot - 1:
            self.iter = None
        self.myrange.set_description(msg)

pbar = ProgressBar(len(l))
for i in l:
    loss = random.random()
    time.sleep(0.01)
    pbar(f'ML (loss={loss:.2f}) Acc={loss:.2f}')

for i in l:
    loss = random.random()
    time.sleep(0.01)
    pbar(f'ML (loss={loss:.2f}) Acc={loss:.2f}')
