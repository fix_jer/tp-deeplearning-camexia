import math
import functools
import operator
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init


def linear(dim_in, dim_out):
    return [nn.Linear(dim_in, dim_out)]

def linear_relu(dim_in, dim_out):
    return [nn.Linear(dim_in, dim_out),
            nn.ReLU(inplace=True)]

def dropout_linear(dim_in, dim_out, p_drop):
    return [nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out)]

def dropout_linear_relu(dim_in, dim_out, p_drop):
    return [nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out),
            nn.ReLU(inplace=True)]

def bn_dropout_linear(dim_in, dim_out, p_drop):
    return [nn.BatchNorm1d(dim_in),
            nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out)]

def bn_dropout_linear_relu(dim_in, dim_out, p_drop):
    return bn_dropout_linear(dim_in, dim_out, p_drop) + [nn.ReLU(inplace=True)]

def conv_relu_maxp(in_channels, out_channels, ks):
    return [nn.Conv2d(in_channels, out_channels,
                      kernel_size=ks,
                      stride=1,
                      padding=int((ks-1)/2), bias=True),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2)]

def conv_bn_relu(in_channels, out_channels, ks):
    return [nn.Conv2d(in_channels, out_channels,
                      kernel_size=ks,
                      stride=1,
                      padding=int((ks-1)/2), bias=True),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)]


class LinearNet(nn.Module):
    """
    A linear network

        input_size : tuple (C, H, W)
        num_classes : int
        l2_reg : float
        dropout : unused
        """
    def __init__(self, input_size, num_classes, l2_reg, dropout):
        super().__init__()
        self.l2_reg = l2_reg
        tot_input_size = functools.reduce(operator.mul, input_size)
        self.classifier = nn.Linear(tot_input_size, num_classes)

    def penalty(self):
        l2_val = 0.0
        for m in self.model.children():
            if isinstance(m, nn.Linear):
                l2_val = m.weights.norm(2) + l2_val
        return self.l2_reg * l2_val

    def forward(self, x):
        x = x.view(x.size()[0], -1)
        y = self.classifier(x)
        return y


class FullyConnected(nn.Module):
    """
    A fully connnected 3 layer network

        input_size : tuple (C, H, W)
        num_classes : int
        l2_reg : float
        """
    def __init__(self, input_size, num_classes, l2_reg, dropout):
        super().__init__()
        self.l2_reg = l2_reg
        tot_input_size = functools.reduce(operator.mul, input_size)

        if dropout is not None:
            block_builder = lambda lin,lout: dropout_linear_relu(lin, lout, dropout)
        else:
            block_builder = lambda lin,lout: linear_relu(lin, lout)

        modules = [
            *linear_relu(tot_input_size, 256),
            *block_builder(256, 256)
        ]
        if dropout is not None:
            modules.append(nn.Dropout(dropout))
        modules.append(nn.Linear(256, num_classes))

        self.model = nn.Sequential(*modules)
        self.init()

    def init(self):
        @torch.no_grad()
        def finit(m):
            if type(m) == nn.Linear:
                torch.nn.init.kaiming_uniform_(m.weight, a=math.sqrt(2.0))
                m.bias.fill_(0.0)
        self.apply(finit)

    def penalty(self):
        l2_val = 0.0
        for m in self.model.children():
            if isinstance(m, nn.Linear):
                l2_val = m.weights.norm(2) + l2_val
        return self.l2_reg * l2_val

    def forward(self, x):
        # We could have used nn.Flatten() for the next line
        x = x.view(x.shape[0], -1)
        y = self.model(x)
        return y


class VanillaCNN(nn.Module):

    def __init__(self, input_size, num_classes, l2_reg, dropout):
        super().__init__()

        self.l2_reg = l2_reg
        self.features = nn.Sequential(
            *conv_relu_maxp(1, 16, 5),
            *conv_relu_maxp(16, 32, 5),
            *conv_relu_maxp(32, 64, 5)
        )
        # You must compute the number of features manualy to instantiate the
        # next FC layer
        # self.num_features = 64*3*3

        # Or you create a dummy tensor for probing the size of the feature maps
        probe_tensor = torch.zeros((1,) + input_size)
        out_features = self.features(probe_tensor).view(-1)
        
        if dropout is not None:
            block_builder = lambda lin,lout: dropout_linear_relu(lin, lout, dropout)
        else:
            block_builder = lambda lin,lout: linear_relu(lin, lout)

        self.classifier = nn.Sequential(
            *block_builder(out_features.shape[0], 128),
            *block_builder(128, 256),
            nn.Linear(256, num_classes)
        )

    def penalty(self):
        l2_val = 0.0
        for m in self.model.children():
            if isinstance(m, nn.Linear) or isinstance(m, nn.Conv2d):
                l2_val = m.weights.norm(2) + l2_val
        return self.l2_reg * l2_val

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size()[0], -1)  #  OR  x = x.view(-1, self.num_features)
        y = self.classifier(x)
        return y


class FancyCNN(nn.Module):
    def __init__(self, input_size, num_classes, l2_reg, dropout):
        super().__init__()

        self.l2_reg = l2_reg
        base_n = 64
        modules = [
            *conv_bn_relu(1, base_n, 3),
            *conv_bn_relu(base_n, base_n, 3),
            nn.MaxPool2d(kernel_size=2),
            *conv_bn_relu(base_n, 2*base_n, 3),
            *conv_bn_relu(2*base_n, 2*base_n, 3),
            nn.MaxPool2d(kernel_size=2), #,
            *conv_bn_relu(2*base_n, 4*base_n, 3),
            *conv_bn_relu(4*base_n, 4*base_n, 3),
            nn.AvgPool2d(kernel_size=7),
            nn.Flatten()
        ]
        if dropout is not None:
            modules.append(nn.Dropout(dropout))
        modules.append(nn.Linear(4*base_n, num_classes))

        self.model = nn.Sequential(*modules)

    def penalty(self):
        l2_val = 0.0
        for m in self.model.children():
            if isinstance(m, nn.Linear) or isinstance(m, nn.Conv2d):
                l2_val = m.weights.norm(2) + l2_val
        return self.l2_reg * l2_val

    def forward(self, x):
        return self.model(x)


import sys
import inspect
available_models = [no[0] for no in inspect.getmembers(sys.modules[__name__]) if inspect.isclass(no[1])]

def build_model(model_name, img_size, num_classes, l2_reg, dropout):
    if model_name not in available_models:
        raise RuntimeError(f"{model_name} is not available. The available models are {available_models}")
    return eval(f"{model_name}(img_size, num_classes, l2_reg, dropout)")

if __name__ == '__main__':
    input_size = (1, 28, 28)
    l2_reg = 0.001
    num_classes = 10
    dummy_input = torch.zeros((1, ) + input_size)
    for m in available_models:
        exec(f"model = {m}(input_size, num_classes, l2_reg, 0.5)")
        out = model(dummy_input)
        print(f"For {m}, the output is of shape {out.shape}")
