import os
from tqdm import trange
import torch


def train(model, loader, f_loss, optimizer, device):
    """
    Train a model for one epoch, iterating over the loader
    using the f_loss to compute the loss and the optimizer
    to update the parameters of the model.

    Arguments :
    model     -- A torch.nn.Module object
    loader    -- A torch.utils.data.DataLoader
    f_loss -- The loss function, i.e. a loss Module
    optimizer -- A torch.optim.Optimzer object
    use_gpu  -- Boolean, whether to use GPU

    Returns :

    """

    # We enter train mode. This is useless for the linear model
    # but is important for layers such as dropout, batchnorm, ...
    pbar = ProgressBar(len(loader))
    model.train()
    N = 0
    tot_loss, correct = 0.0, 0
    for i, (inputs, targets) in enumerate(loader):

        inputs, targets = inputs.to(device), targets.to(device)

        # Compute the forward propagation
        outputs = model(inputs)

        loss = f_loss(outputs, targets)

        # Accumulate the number of processed samples
        N += inputs.shape[0]

        # For the total loss
        tot_loss += inputs.shape[0] * loss.item()

        # For the total accuracy
        predicted_targets = outputs.argmax(dim=1)
        correct += (predicted_targets == targets).sum().item()

        # Backward and optimize
        optimizer.zero_grad()
        loss.backward()
        try:
            model.penalty().backward()
        except AttributeError:
            pass
        optimizer.step()

        # Display status
        pbar(f"Train Loss : {tot_loss/N:.4f}, Acc : {correct/N:.4f}")
    return tot_loss / N, correct / N


def test(model, loader, f_loss, device):
    """
    Test a model by iterating over the loader

    Arguments :

        model     -- A torch.nn.Module object
        loader    -- A torch.utils.data.DataLoader
        f_loss    -- The loss function, i.e. a loss Module
        device    -- a torch.device object

    Returns :

        A tuple with the mean loss and mean accuracy

    """
    # We disable gradient computation which speeds up the computation
    # and reduces the memory usage
    with torch.no_grad():
        # We enter evaluation mode. This is useless for the linear model
        # but is important with layers such as dropout, batchnorm, ..
        model.eval()
        N = 0
        tot_loss, correct = 0.0, 0
        for i, (inputs, targets) in enumerate(loader):

            inputs, targets = inputs.to(device), targets.to(device)

            outputs = model(inputs)

            loss = f_loss(outputs, targets)

            N += inputs.shape[0]

            # For the loss
            tot_loss += inputs.shape[0] * loss.item()

            # For the accuracy
            predicted_targets = outputs.argmax(dim=1)
            correct += (predicted_targets == targets).sum().item()
        return tot_loss / N, correct / N


class ProgressBar:
    def __init__(self, tot):
        self.tot = tot
        self.enter()

    def enter(self):
        self.myrange = trange(self.tot)
        self.iter = iter(self.myrange)

    def __call__(self, msg):
        if self.iter is None:
            self.enter()
        j = next(self.iter)
        if j == self.tot - 1:
            self.iter = None
        self.myrange.set_description(msg)


def generate_unique_logpath(logdir, raw_run_name):
    i = 0
    while True:
        run_name = raw_run_name + "_" + str(i)
        log_path = os.path.join(logdir, run_name)
        if not os.path.isdir(log_path):
            return log_path
        i = i + 1


class ModelCheckpoint:
    def __init__(self, filepath, dict_to_save):
        self.min_loss = None
        self.filepath = filepath
        self.dict_to_save = dict_to_save

    def update(self, loss):
        if (self.min_loss is None) or (loss < self.min_loss):
            print(">>>>>> Saving a better model <<<<<<<<<")
            # torch.save(self.model.state_dict(), self.filepath)
            torch.save(self.dict_to_save, self.filepath)
            self.min_loss = loss
