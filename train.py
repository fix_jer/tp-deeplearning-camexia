# coding: utf-8

# Standard imports
import argparse
import os
import glob

# External imports
import torch
import torch.nn as nn
import torch.optim.lr_scheduler as lr_scheduler

# from torch.utils.tensorboard import SummaryWriter
import neptune.new as neptune
import torchinfo

# Local imports
import models
import data
import utils


def main(args):
    """
    Train a neural network on FashionMNIST
    args : dict
    """
    loggers = []
    neptune_logger = None
    if "NEPTUNE_TOKEN" in os.environ and "NEPTUNE_PROJECT" in os.environ:
        print("Using Neptune.ai logger")
        neptune_logger = neptune.init(
            api_token=os.environ["NEPTUNE_TOKEN"],
            project=os.environ["NEPTUNE_PROJECT"],
            source_files=glob.glob("*.py"),
        )
        loggers.append(lambda epoch, key, value: neptune_logger[key].log(value))

    img_width = 28
    img_height = 28
    input_size = (1, img_height, img_width)
    num_classes = 10
    batch_size = 128
    valid_ratio = 0.2

    # Use the GPU if available
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    print(f"Using the device {device}")

    # Where to store the logs
    base_logdir = args["logdir"]
    logdir = utils.generate_unique_logpath(base_logdir, args["model"])
    print(f"Logging to {logdir}")
    if not os.path.exists(base_logdir):
        os.mkdir(base_logdir)
    if not os.path.exists(logdir):
        os.mkdir(logdir)

    # Load the dataloaders
    loaders, fnorm = data.make_dataloaders(
        valid_ratio,
        batch_size,
        args["num_workers"],
        args["normalize"],
        args["data_augment"],
        args["dataset_dir"],
        None,
    )
    train_loader, valid_loader, test_loader = loaders

    # Init model, loss, optimizer
    model = models.build_model(
        args["model"], input_size, num_classes, args["weight_decay"], args["dropout"]
    )
    model.to(device)
    loss = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=args["lr"])

    # Record some info on the logger
    if neptune_logger:
        neptune_logger["config/params"] = args
        neptune_logger["config/model"] = torchinfo.summary(
            model, input_size=(1,) + input_size
        )

    # Callbacks
    model_checkpoint = utils.ModelCheckpoint(
        logdir + "/best_model.pt", {"model": model, "normalization_function": fnorm}
    )
    scheduler = lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.5)

    # Train
    for e in range(args["epochs"]):
        # Train one epoch
        print(f'[Epoch {e}/{args["epochs"]}]')
        train_loss, train_acc = utils.train(
            model, train_loader, loss, optimizer, device
        )

        # Test on the validation set
        val_loss, val_acc = utils.test(model, valid_loader, loss, device)
        print(f"Valid Loss : {val_loss:.4f}, Acc : {val_acc:.4f}")
        # Test on the independent test set
        test_loss, test_acc = utils.test(model, test_loader, loss, device)
        print(f"Test  Loss : {test_loss:.4f}, Acc : {test_acc:.4f}")

        # Update the best model
        model_checkpoint.update(val_loss)

        # Update the learning rate if necessary
        scheduler.step()

        # Save the metrics on the board of neptune
        for log in loggers:
            log(e, "train/accuracy", train_acc)
            log(e, "train/celoss", train_loss)
            log(e, "valid/accuracy", val_acc)
            log(e, "valid/celoss", val_loss)
            log(e, "test/accuracy", test_acc)
            log(e, "test/celoss", test_loss)
            log(e, "learning_rate", scheduler.get_last_lr())
        print("------")


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--logdir", type=str, help="Where to store the logs", default="./logs"
    )

    parser.add_argument(
        "--commit_id",
        type=str,
        help="The commit_id to store which version of the code is used",
        default=None,
    )

    parser.add_argument(
        "--dataset_dir",
        type=str,
        help="Where to store the downloaded dataset",
        default=None,
    )

    parser.add_argument(
        "--num_workers", type=int, default=7, help="The number of CPU threads used"
    )

    parser.add_argument("--lr", type=float, default=0.001, help="Learning rate")

    parser.add_argument("--weight_decay", type=float, default=0, help="Weight decay")

    parser.add_argument(
        "--dropout", type=float, default=None, help="Dropout factor before FC layers"
    )

    parser.add_argument(
        "--data_augment",
        help="Specify if you want to use data augmentation",
        action="store_true",
    )

    parser.add_argument(
        "--normalize",
        help="Which normalization to apply to the input data",
        action="store_true",
    )

    parser.add_argument(
        "--epochs", type=int, help="The number of epochs to train for", default=40
    )

    parser.add_argument(
        "--model",
        type=str,
        choices=models.available_models,
        action="store",
        required=True,
    )

    args = parser.parse_args()
    main(vars(args))
